/*======================
WD005-S1-D1 DOM MANIPULATION

A) DOCUMENT OBJECT MODEL (DOM)
- a ___representation___ by the browser that enables the HTML structure to be 
easily accessed by style sheets and programming languages.

- See DOM VIEWER: https://software.hixie.ch/utilities/js/live-dom-viewer/

- __Tree-like__ network of nodes: 

- to the DOM, everything on a web page is a ___node___.

1. __ Element__ NODE
- an element, as it exists in the DOM

2. ROOT NODE
- the __HTML__ tag is the root node, with every other part of 
the document being a child node

3. CHILD NODE
- a node ___directly___  inside another node .

4. DESCENDANT NODE
-  A node __anywhere__ inside another node. 

5. ___Parent___ NODE
- A node which has another node inside it.

6. SIBLING NODES
- Nodes that sit on the same ___level___ in the DOM tree.

7. TEXT NODE
- A node containing a __string__ .

In JS, we can access and modify different parts of a web page 
using a special built-in object called ____DOCUMENT_____.


B) DOM MANIPULATION
==========================*/

console.log('************ LEGACY METHODS ************');
/*==========================
1) LEGACY DOM SHORTCUT METHODS
- the following are considered "legacy" because they are from an ___earlier___ version 
of the DOM but are still working in the current version
==========================*/
console.log(`>>>>>>>  console.log(document)`);
console.log(document);
console.log(``);

console.log(`>>>>>>>>   console.log(document.body);`);
console.log(document.body);
console.log(``);

console.log(`>>>>>>>>   console.log(document.images);`);
console.log(document.images);
console.log(``);

console.log(`>>>>>>>>   console.log(document.links);`);
console.log(document.links);	//node list of all <a> elements
console.log(`comment-->>>>>>>> //node list of all <a> elements`)
console.log(``);

console.log(`>>>>>>>>  console.log(document.anchors);`);
console.log(document.anchors);	//return a node list of all <a> tagged elements with name attribute.
console.log(`comment-->>>>>>>> //return a node list of all <a> tagged elements WITH "name" attribute.`)
console.log(``);

console.log(`>>>>>>>>  console.log(document.forms);`);
console.log(document.forms);
console.log(``);


console.log('************ NODE LIST ************');
//NODE LISTS are ___array-like___ objects in that they follow index notation

console.log(`>>>>>>>> console.log(document.links[0]);`);
console.log(document.links[0]);
console.log(``);

console.log(`>>>>>>>>   console.log(document.links.length)`);
console.log(document.links.length);
console.log(``);

console.log(`comment-->>>>>>>>> for loop to iterate all the links in the document`);
console.log(`console.log(document.links[i]);`);
console.log(`console.log(document.links[i].innerText);`);
for(let i = 0; i < document.links.length; i++){
	console.log(document.links[i]);
	console.log(document.links[i].innerText);		//'innerText' is a property of links
}
console.log(`>>>>>>>>   end of for loop   <<<<<<<<`);
console.log(``);

console.log(`>>>>>>>>   let nodeList = document.links`)
let nodeList = document.links;
console.log(`>>>>>>>>   console.log(nodeList);`);
console.log(nodeList);
console.log(``);

console.log(`console.log(nodeList.pop())`);
console.log(`pop() method will result to error because 'documents.links' is a NODE (object) not an array`);
//console.log(nodeList.pop());		//not array but objects (can't do pop() method);
console.log(``);

console.log(`>>>>>>>>   //spread operator [...] to make an object to an array`)
console.log(`>>>>>>>>   nodeList = [...document.links];`)
nodeList = [...document.links];		//spread operator [...] to make an object to an array
console.log(`>>>>>>>>   console.log(nodeList);`);
console.log(nodeList);
console.log(``);
console.log(`>>>>>>>>   now, able to perform array's pop() method`)
console.log(`>>>>>>>>   console.log(nodeList.pop());`);
console.log(nodeList.pop());
console.log(`>>>>>>>>   console.log(nodeList);`);
console.log(nodeList);
console.log(``);


console.log('*********** METHODS TO ACCESS ELEMENTS *************');
/*========================
2) METHODS TO ACCESS ELEMENTS
- the DOM provides several methods for us to access any element on a page
- the said methods will either return a node object or a node list 
- as an example, let's access the body element of a web page and assign 
it to a variable named body
========================*/
console.log(`>>>>>>>>    let body = document.body;`);
let body = document.body;
console.log(`>>>>>>>>    console.log(body);`);
console.log(body);
console.log(``);

console.log(`>>>>>>>>    console.log(typeof body);`);
console.log(typeof body);		// object
console.log(``);

console.log(`>>>>>>>>    console.log(body.nodeType);`);
console.log(body.nodeType);		//meaning: 1=body, 2=attribute, 3=text, 8=comment, 9=document
console.log(`comment-->>>>>>>>    //meaning: 1=body, 2=attribute, 3=text, 8=comment, 9=document`);
console.log(``);

console.log(`>>>>>>>>    console.log(body.nodeName)`);
console.log(body.nodeName);		// BODY
console.log(``);


console.log('*********** GET ELEMENT BY ITS ID *************');
/*=======================
the getElementById() method returns a reference to the element 
with a __Unique__id attribute matching the passed in argument
======================= */
console.log(`declare-->>>>>>>>    const title = document.getElementById("title");`);
const title = document.getElementById("title");
console.log(`>>>>>>>>    console.log(title);`);
console.log(title);
console.log(``);

console.log(`>>>>>>>>    console.log(title.innerText);`);
console.log(title.innerText);		//text only
console.log(`notes-->>>>>>>>    //text only`);
console.log(``);

console.log(`>>>>>>>>    console.log(title.innerHTML);`);
console.log(title.innerHTML);		//text and including the html nested element (tag)
console.log(`notes-->>>>>>>>    //text and including the html nested element (tag)`);
console.log(``);

console.log(`>>>>>>>>    const test = document.getElementById("hero");`);
const test = document.getElementById("hero");
console.log(`>>>>>>>>    console.log(test);`);
console.log(test);									//null because 'hero' has no id
console.log(`>>>>>>>>    //null because no element has 'hero' id`);
console.log(``);


console.log('*********** GET ELEMENTS BY THEIR TAG NAME *************');
/*=======================
getElementsByTagName() method returns a __live__ node list of all the elements 
with the passed in tag name argument
======================= */
console.log(`>>>>>>>>    const listItem = document.getElementsByTagName('li');`);
const listItem = document.getElementsByTagName('li');	//accessing the 'document' object

console.log(`>>>>>>>>    const listItem2 = document.getElementsByTagName('lis');`);
const listItem2 = document.getElementsByTagName('lis');	//non-existing tag returns empty HTMLCollection array
console.log(``);

console.log(`>>>>>>>>    console.log(listItem);`);
console.log(listItem);		//returns a node list (node is like the linkedlist data structure);
console.log(`notes-->>>>>>>>    //returns a node list (node is like the linkedlist data structure);`);
console.log(``);

console.log(`>>>>>>>>    console.log(listItem2);`);
console.log(listItem2);		//returns an empty node list since tag 'lis' is not found
console.log(`>>>>>>>>    //returns an empty node list since tag 'lis' is not found`);
console.log(``);

console.log(`>>>>>>>>    const listItemsArr = [...listItem];`);
const listItemsArr = [...listItem];
console.log(``);

console.log(`>>>>>>>>    console.log(listItemsArr);`);
console.log(listItemsArr);
console.log(``);


console.log('*********** GET ELEMENTS BY THEIR CLASS NAME *************');
/*=======================
getElementsByClassName() method returns a ___LIVE____ node list of all the elements with the passed in class name argument
======================= */

console.log(`>>>>>>>>    const heroes = document.getElementsByClassName('hero');`);
const heroes = document.getElementsByClassName('hero');
console.log(`>>>>>>>>    console.log(heroes);	`);
console.log(heroes);					//nodelist:  HTMLCollection(2) [li.hero, li.hero]
console.log(``);

console.log(`>>>>>>>>    const heroesArr = [...heroes];`);
const heroesArr = [...heroes];
console.log(`>>>>>>>>    console.log(heroesArr);	`);
console.log(heroesArr);					//array: 	(2) [li.hero, li.hero]
console.log(``);


console.log('*********** LIVE COLLECTIONS *************');
/*======================
- the node lists returned by document.getElementsByClassName() 
and document.getElementsByTagName() are both ___LIVE___
- "live" means collections will ___UPDATE___ to reflect any changes on the page
- if elements are added or an existing one is removed, node lists 
returned by these methods will reflect the 
change ____automatically____ without making another call to the method
=======================*/
console.log(`>>>>>>>>    const heroesRoster = document.getElementById('roster');`);
const heroesRoster = document.getElementById('roster');
console.log(`>>>>>>>>    console.log(heroesRoster);`);
console.log(heroesRoster);
console.log(``);

console.log(`>>>>>>>>    console.log(heroesRoster.children)`);
console.log(heroesRoster.children);
console.log(``);

console.log(`>>>>>>>>    const list = heroesRoster.children;`);
const list = heroesRoster.children;
console.log(`>>>>>>>>    console.log(list.length);`);
console.log(list.length);			//3
console.log(``);

console.log(`>>>>>>>>    const brucewayne = document.getElementById("bat");`);
const brucewayne = document.getElementById("bat");
console.log(``);

console.log(`>>>>>>>>    heroesRoster.removeChild(brucewayne);`);
heroesRoster.removeChild(brucewayne);			//removeChild (method to an object)
console.log(`comment -->>>>>>>>    //removeChild (METHOD to an OBJECT)`);
console.log(``);

console.log(`>>>>>>>>    console.log(list.length);`);
console.log(list.length);	//2
console.log(``);


console.log(`>>>>>>>>    console.log(heroesRoster.children[2]);`);
console.log(heroesRoster.children[2]);
console.log(``);



console.log('*********** QUERY SELECTOR *************');
/*=======================
- querySelector() method allows you to use CSS notation to 
find the ___FIRST___ element in the document that matches the passed in CSS selector

- this is the ___recommended____ modern approach, which is convenient 
because it allows you to select elements using CSS selectors. 
======================= */

console.log(`>>>>>>>>    console.log(document.querySelector("#roster"));`);
console.log(document.querySelector("#roster"));
console.log(``);

console.log(`>>>>>>>>    console.log(document.querySelector("roster"));`);
console.log(document.querySelector("roster"));	//null
console.log(``);

console.log(`>>>>>>>>    console.log(document.querySelector(".hero"));`);
console.log(document.querySelector(".hero"));	//will return only the "first element"
console.log(``);

console.log(`>>>>>>>>    console.log(document.querySelectorAll(".hero"));`);
console.log(document.querySelectorAll(".hero"));	//will return all elements with class 'hero'
console.log(``);

console.log(`>>>>>>>>    console.log(document.querySelector(".hero"));`);
console.log(document.querySelector(".hero"));
console.log(``);

console.log(`>>>>>>>>    console.log(document.querySelector("section"));`);
console.log(document.querySelector("section"));
console.log(``);

console.log(`>>>>>>>>    console.log(document.querySelector("li:last-child"));`);
console.log(document.querySelector("li:last-child"));
console.log(``);

console.log(`>>>>>>>>    console.log(document.querySelector("ul#roster"));`);
console.log(document.querySelector("ul#roster"));
console.log(``);



console.log('*********** QUERY SELECTOR ALL  *************');
/*=======================
document.querySelectorAll() works in the same way except it 
returns a node list of ________ elements that match the passed in CSS selector
======================= */


/*=======================
- unlike the previous methods that return live node lists, 
querySelectorAll _________________ return a live node list
- both these methods can emulate all previously discussed methods with the added advantage of offering more control over element selection due to the use of CSS selectors
======================= */


console.log('*********** CREATE AND PLACE NEW NODES *************');
/*========================
3) CREATE AND PLACE NEW NODES
a) document.createElement() method 
b) Node.textContent property
c) Node.appendChild() method
d) Node.prepend() method
e) Document.createTextNode():
========================*/
console.log(`>>>>>>>>    let section = document.querySelector("section");`);
let section = document.querySelector("section");		//no pound sign because 'section' is an element/tag
console.log(`>>>>>>>>    let p = document.createElement('p');`);
let p = document.createElement('p');				//paragraph element <p> tag
console.log(`>>>>>>>>    console.log(p);`);
console.log(p);
console.log(``);

console.log(`>>>>>>>>    p.textContent = "This is a sample appended paragraph.";`);
p.textContent = "This is a sample appended paragraph.";
console.log(`>>>>>>>>    console.log(p);`);
console.log(p);
console.log(``);

console.log(`>>>>>>>>    section.appendChild(p);`);
section.appendChild(p);		//dynamically add the paragraph to the webpage (not in index.html)
console.log(`>>>>>>>>    console.log(section);`);
console.log(section);		//append: // sa baba - before ng end tag <section>
console.log(`comment -->>>>>>>>    //append: // sa baba - before ng end tag <section>`);
console.log(``);

console.log(`>>>>>>>>    let p2 = document.createElement('p');`);
let p2 = document.createElement('p');
console.log(`>>>>>>>>    p2.textContent = "This is a sample prepended paragraph.";`);
p2.textContent = "This is a sample prepended paragraph.";
console.log(`>>>>>>>>    section.prepend(p2);`);
section.prepend(p2);			// prepend: sa taas - after ng beginning tag <section>
console.log(`comment -->>>>>>>>    // prepend: sa taas - after ng beginning tag <section>`);
console.log(``);

console.log(`>>>>>>>>    let author = document.createTextNode(" - JP, 2019");`);
let author = document.createTextNode(" - JP, 2019");
console.log(`>>>>>>>>    p.appendChild(author);`);
p.appendChild(author);			//yung 'createTextNode' ay inappend kay paragraph element (na nasa loob ng p variable)
console.log(``);


console.log('*********** MOVE AND REMOVE NODES *************');
/*========================
4) MOVE AND REMOVE NODES
a) Node.removeChild()
========================*/
console.log(`>>>>>>>>    section.removeChild(p2);`);
section.removeChild(p2);
console.log(`>>>>>>>>    console.log(section);`);
console.log(section);
console.log(``);


console.log('*********** MANIPULATE STYLES *************');
/*========================
5) MANIPULATING STYLES
a) HTMLElement.style property,
b) Element.setAttribute() 

Notice how the JavaScript property versions of the CSS styles are written in ___________ whereas 
the CSS versions are hyphenated (e.g. backgroundColor versus background-color). 
========================*/
console.log(`>>>>>>>>    p.style.color = "green";`);
p.style.color = "green";
console.log(`>>>>>>>>    p.style.backgroundColor = "black";`);
p.style.backgroundColor = "black";		//in JS, using camel-case backgroundColor | while in css it is background-color
console.log(`>>>>>>>>    p.style.padding = "10px";`);
p.style.padding = "10px";
console.log(`>>>>>>>>    p.style.width = "300px";`);
p.style.width = "300px";
console.log(`>>>>>>>>    p.style.textAlign = "center";`);
p.style.textAlign = "center";
console.log(`>>>>>>>>    console.log(p.style);`);
console.log(p.style);
console.log(``);

//const heroesRoster = document.getElementById('roster');
console.log(`declared earlier-->>>>>>>>    //const heroesRoster = document.getElementById('roster');`);
console.log(`>>>>>>>>    const superman = heroesRoster.children[0];`);
const superman = heroesRoster.children[0];
console.log(`>>>>>>>>    console.log(superman);`);
console.log(superman);
console.log(``);

console.log(`>>>>>>>>    superman.setAttribute("class", "btn btn-lg btn-warning");`);
superman.setAttribute("class", "btn btn-lg btn-warning");
console.log(``);

console.log(`>>>>>>>>    const wonderWoman = document.querySelector("li:last-child");`);
const wonderWoman = document.querySelector("li:last-child");
console.log(`>>>>>>>>    wonderWoman.setAttribute("class", "test");`);
wonderWoman.setAttribute("class", "test");
console.log(`>>>>>>>>    console.log(wonderWoman);`);
console.log(wonderWoman);
console.log(``);




console.log('*********** DOM EVENTS *************');
/*========================
5) DOM EVENTS
addEventListener() sets up a function that will be 
called whenever the specified __event__ is delivered to the 
target (e.g., document object)

https://developer.mozilla.org/en-US/docs/Web/Events
========================*/

console.log(`>>>>>>>>    2 ways: addEventListener and document.querySelector`);
console.log(``);

//const title = document.getElementById("title");
console.log(`declared earlier-->>>>>>>>    //const title = document.getElementById("title");`);
console.log(``);

console.log(`>>>>>>>>    document.querySelector("#title").onclick = function(){`);
document.querySelector("#title").onclick = function(){
	title.style.color = "blue"			//change font color upon click
}
console.log(``);

console.log(`>>>>>>>>    title.addEventListener("click", function(event) {`);
title.addEventListener("click", function(event) {
	alert("I used addEventListener!");
})
console.log(``);