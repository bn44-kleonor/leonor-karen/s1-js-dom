/*======================
WD005-S1-D1 DOM MANIPULATION

A) DOCUMENT OBJECT MODEL (DOM)
- a ___representation___ by the browser that enables the HTML structure to be 
easily accessed by style sheets and programming languages.

- See DOM VIEWER: https://software.hixie.ch/utilities/js/live-dom-viewer/

- __Tree-like__ network of nodes: 

- to the DOM, everything on a web page is a ___node___.

1. __ Element__ NODE
- an element, as it exists in the DOM

2. ROOT NODE
- the __HTML__ tag is the root node, with every other part of 
the document being a child node

3. CHILD NODE
- a node ___directly___  inside another node .

4. DESCENDANT NODE
-  A node __anywhere__ inside another node. 

5. ___Parent___ NODE
- A node which has another node inside it.

6. SIBLING NODES
- Nodes that sit on the same ___level___ in the DOM tree.

7. TEXT NODE
- A node containing a __string__ .

In JS, we can access and modify different parts of a web page 
using a special built-in object called ____DOCUMENT_____.


B) DOM MANIPULATION
==========================*/

console.log('************ LEGACY METHODS ************');
/*==========================
1) LEGACY DOM SHORTCUT METHODS
- the following are considered "legacy" because they are from an ___earlier___ version 
of the DOM but are still working in the current version
==========================*/
console.log(document);
console.log(document.body);
console.log(document.images);
console.log(document.links);	//node list of all <a> elements
console.log(document.anchors);	//return a node list of all <a> tagged elements with name attribute.
console.log(document.forms);



console.log('************ NODE LIST ************');
//NODE LISTS are ___array-like___ objects in that they follow index notation
console.log(document.links[0]);
console.log(document.links.length);
for(let i = 0; i < document.links.length; i++){
	console.log(document.links[i]);
	console.log(document.links[i].innerText);		//properties of links e.g. innerText
}

let nodeList = document.links;
//console.log(nodeList.pop());		//not array but objects (can't do pop() method);

nodeList = [...document.links];		//spread operator [...] to make an object to an array
console.log(nodeList.pop());
console.log(nodeList);


console.log('*********** METHODS TO ACCESS ELEMENTS *************');
/*========================
2) METHODS TO ACCESS ELEMENTS
- the DOM provides several methods for us to access any element on a page
- the said methods will either return a node object or a node list 
- as an example, let's access the body element of a web page and assign 
it to a variable named body
========================*/
let body = document.body;
console.log(typeof body);		// object
console.log(body.nodeType);		// 1=meaning body, 2=attribute, 3=text, 8=comment, 9=document
console.log(body.nodeName);		// BODY


console.log('*********** GET ELEMENT BY ITS ID *************');
/*=======================
the getElementById() method returns a reference to the element 
with a __Unique__id attribute matching the passed in argument
======================= */
const title = document.getElementById("title");
console.log(title);
console.log(title.innerText);		//text only
console.log(title.innerHTML);		//text and including the html nested element (tag)

const test = document.getElementById("hero");
console.log(test);									//null because 'hero' has no id


console.log('*********** GET ELEMENTS BY THEIR TAG NAME *************');
/*=======================
getElementsByTagName() method returns a __live__ node list of all the elements 
with the passed in tag name argument
======================= */
const listItem = document.getElementsByTagName('li');	//accessing the 'document' object
const listItem2 = document.getElementsByTagName('lis');	//non-existing tag returns empty HTMLCollection array
console.log(listItem);		//returns a node list (node is like the linkedlist data structure);
console.log(listItem2);		//returns an empty node list
const listItemsArr = [...listItem];
console.log(listItemsArr);



console.log('*********** GET ELEMENTS BY THEIR CLASS NAME *************');
/*=======================
getElementsByClassName() method returns a ___LIVE____ node list of all the elements with the passed in class name argument
======================= */
const heroes = document.getElementsByClassName('hero');
console.log(heroes);					//nodelist:  HTMLCollection(2) [li.hero, li.hero]
const heroesArr = [...heroes];
console.log(heroesArr);					//array: 	(2) [li.hero, li.hero]



console.log('*********** LIVE COLLECTIONS *************');
/*======================
- the node lists returned by document.getElementsByClassName() 
and document.getElementsByTagName() are both ___LIVE___
- "live" means collections will ___UPDATE___ to reflect any changes on the page
- if elements are added or an existing one is removed, node lists 
returned by these methods will reflect the 
change ____automatically____ without making another call to the method
=======================*/
const heroesRoster = document.getElementById('roster');
console.log(heroesRoster);
console.log(heroesRoster.children);
const list = heroesRoster.children;
console.log(list.length);			//3
const brucewayne = document.getElementById("bat");
heroesRoster.removeChild(brucewayne);			//removeChild (method to an object)
console.log(list.length);	//2
console.log(heroesRoster.children[2]);



console.log('*********** QUERY SELECTOR *************');
/*=======================
- querySelector() method allows you to use CSS notation to 
find the ___FIRST___ element in the document that matches the passed in CSS selector

- this is the ___recommended____ modern approach, which is convenient 
because it allows you to select elements using CSS selectors. 
======================= */

console.log(document.querySelector("#roster"));
console.log(document.querySelector("roster"));	//null
console.log(document.querySelector(".hero"));	//will return only the "first element"
console.log(document.querySelectorAll(".hero"));	//will return all elements with class 'hero'

console.log(document.querySelector(".hero"));
console.log(document.querySelector("section"));
console.log(document.querySelector("li:last-child"));
console.log(document.querySelector("ul#roster"));



console.log('*********** QUERY SELECTOR ALL  *************');
/*=======================
document.querySelectorAll() works in the same way except it 
returns a node list of ________ elements that match the passed in CSS selector
======================= */


/*=======================
- unlike the previous methods that return live node lists, 
querySelectorAll _________________ return a live node list
- both these methods can emulate all previously discussed methods with the added advantage of offering more control over element selection due to the use of CSS selectors
======================= */


console.log('*********** CREATE AND PLACE NEW NODES *************');
/*========================
3) CREATE AND PLACE NEW NODES
a) document.createElement() method 
b) Node.textContent property
c) Node.appendChild() method
d) Node.prepend() method
e) Document.createTextNode():
========================*/
let section = document.querySelector("section");		//no pound sign because 'section' is an element/tag
let p = document.createElement('p');				//paragraph element <p> tag
console.log(p);
p.textContent = "This is a sample appended paragraph.";
console.log(p);
section.appendChild(p);		//dynamically add the paragraph to the webpage (not in index.html)
//console.log(section);		//append: // sa baba - before ng end tag <section>

let p2 = document.createElement('p');
p2.textContent = "This is a sample prepended paragraph.";
section.prepend(p2);			// prepend: sa taas - after ng beginning tag <section>

let author = document.createTextNode(" - JP, 2019");
p.appendChild(author);			//yung 'createTextNode' ay inappend kay paragraph (p variable


console.log('*********** MOVE AND REMOVE NODES *************');
/*========================
4) MOVE AND REMOVE NODES
a) Node.removeChild()
========================*/
section.removeChild(p2);



console.log('*********** MANIPULATE STYLES *************');
/*========================
5) MANIPULATING STYLES
a) HTMLElement.style property,
b) Element.setAttribute() 

Notice how the JavaScript property versions of the CSS styles are written in ___________ whereas 
the CSS versions are hyphenated (e.g. backgroundColor versus background-color). 
========================*/
p.style.color = "green";
p.style.backgroundColor = "black";		//in JS, using camel-case backgroundColor | while in css it is background-color
p.style.padding = "10px";
p.style.width = "300px";
p.style.textAlign = "center";
console.log(p.style);

//const heroesRoster = document.getElementById('roster');
const superman = heroesRoster.children[0];
//console.log(superman);

superman.setAttribute("class", "btn btn-lg btn-warning");

const wonderWoman = document.querySelector("li:last-child");
wonderWoman.setAttribute("class", "test");




console.log('*********** DOM EVENTS *************');
/*========================
5) DOM EVENTS
addEventListener() sets up a function that will be 
called whenever the specified __event__ is delivered to the 
target (e.g., document object)

https://developer.mozilla.org/en-US/docs/Web/Events
========================*/

//const title = document.getElementById("title");
document.querySelector("#title").onclick = function(){
	title.style.color = "blue"			//change font color upon click
}

title.addEventListener("click", function(event) {
	alert("I used addEventListener!");
})

